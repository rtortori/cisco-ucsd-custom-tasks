importPackage(java.lang); 
importPackage(java.io); 
importPackage(com.cloupia.lib.util); 
importClass(org.apache.http.conn.ssl.SSLConnectionSocketFactory); 
importClass(org.apache.http.conn.ssl.SSLContextBuilder); 
importClass(org.apache.http.HttpHost); 
importClass(org.apache.http.impl.client.CloseableHttpClient); 
importClass(org.apache.http.conn.ssl.TrustSelfSignedStrategy); 
importClass(org.apache.http.impl.conn.DefaultProxyRoutePlanner); 
importClass(org.apache.http.impl.client.HttpClients); 
importClass(org.apache.http.impl.client.CloseableHttpClient); 
importClass(org.apache.http.client.methods.HttpPost); 
importClass(org.apache.http.client.methods.HttpGet); 
importClass(org.apache.http.client.methods.HttpPut); 
importClass(org.apache.http.entity.StringEntity); 

var uri = "/api/v2/cmdb/system/interface"; 

var firewall = input.fortigate_firewall; 
var port = input.tcp_port; 
var api_key = input.api_key; 
var vdom_name = input.VDOM_Name; 
var interface_name = input.interface_name;
var ip_address = input.ip_adddress;
var netmask = input.netmask;
var allowaccess = input.type_access;

var builder = new SSLContextBuilder(); 
builder.loadTrustMaterial(null, new TrustSelfSignedStrategy()); 
var sslsf = new SSLConnectionSocketFactory(builder.build(),SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER); 
var httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build(); 
var request = new HttpPut( "https://" + firewall + ":" + port + "/" + uri + "/" + interface_name);

var payload = '{"name": "' + interface_name + '","vdom": "' + vdom_name + '","mode": "static","management-ip": "0.0.0.0 0.0.0.0","ip": "'+ ip_address + ' ' + netmask + '","allowaccess": "' + allowaccess + '"}';

request.addHeader("Content-Type", "application/json"); 
request.addHeader("Authorization", "Bearer " + api_key); 
request.setEntity( new StringEntity( payload ) ); 

var response = httpclient.execute(request); 
httpStatus = response.getStatusLine().getStatusCode(); 
var hdrs = response.getAllHeaders(); 

logger.addInfo("Headers: " + hdrs.length ); 
logger.addInfo("VDOM Name: " + vdom_name); 
logger.addInfo("HTTP Status: " + httpStatus); 
logger.addInfo("API Key is: " + api_key);
logger.addInfo("Interface name is: " + interface_name);
logger.addInfo("IP Address is: " + ip_address);
logger.addInfo("Netmask is: " + netmask); 
logger.addInfo("Allowaccess is: " + allowaccess);
logger.addInfo("Payload is: " + payload)

if (httpStatus != 200) { 
    ctxt.setFailed("Unable to configure the interface, HTTP response code: " + httpStatus); 
    ctxt.exit(); 
} else { 
    ctxt.setSuccessful(); 
} 

request.releaseConnection(); 