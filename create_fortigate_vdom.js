// Create Fortigate VDOM
importPackage(java.lang);
importPackage(java.io);
importPackage(com.cloupia.lib.util);
importClass(org.apache.http.conn.ssl.SSLConnectionSocketFactory);
importClass(org.apache.http.conn.ssl.SSLContextBuilder);
importClass(org.apache.http.HttpHost);
importClass(org.apache.http.impl.client.CloseableHttpClient);
importClass(org.apache.http.conn.ssl.TrustSelfSignedStrategy);
importClass(org.apache.http.impl.conn.DefaultProxyRoutePlanner);
importClass(org.apache.http.impl.client.HttpClients);
importClass(org.apache.http.impl.client.CloseableHttpClient);
importClass(org.apache.http.client.methods.HttpPost);
importClass(org.apache.http.client.methods.HttpGet);
importClass(org.apache.http.entity.StringEntity);

// Fortigate endpoint URI
var uri = "/api/v2/cmdb/system/vdom";
// Getting vars from workflow inputs
var firewall = input.fortigate_firewall;
var port = input.tcp_port;
var api_key = input.api_key;
var vdom_name = input.VDOM_Name;

var builder = new SSLContextBuilder();
builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());
var sslsf = new SSLConnectionSocketFactory(builder.build(),SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
var httpclient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
var request = new HttpPost( "https://" + firewall + ":" + port + "/" + uri );

request.addHeader("Content-Type", "application/json");
request.addHeader("Authorization", "Bearer " + api_key);
request.setEntity( new StringEntity( "{\"name\":\"" + vdom_name + "\"}" ) );

var response = httpclient.execute(request);
httpStatus = response.getStatusLine().getStatusCode();
var hdrs = response.getAllHeaders();

logger.addInfo("Headers: " + hdrs.length );
logger.addInfo("VDOM Name: " + vdom_name);
logger.addInfo("HTTP Status: " + httpStatus);
logger.addInfo("API Key is: " + api_key);

if (httpStatus != 200) {
	ctxt.setFailed("Unable to create VDOM, HTTP response code: " + httpStatus);
	ctxt.exit();
} else {
	ctxt.setSuccessful();
}

request.releaseConnection(); 